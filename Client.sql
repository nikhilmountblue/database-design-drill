CREATE DATABASE Project2;
USE Project2;


CREATE TABLE `Client Table`(
`Client id` INT PRIMARY KEY,
`First Name` TEXT NOT NULL,
`Last Name` TEXT ,
`Address` TEXT NOT NULL,
`City` TEXT NOT NULL,
`Zip code` INT NOT NULL,
`State` TEXT NOT NULL,
`Contact-Number1` INT NOT NULL,
`Contact-Number2` INT
);


CREATE TABLE `Contract Table`(
`Contractid` INT PRIMARY KEY,
`Client id` INT NOT NULL,
`Manager id` INT NOT NULL,
`Contract details` TEXT,
`Contract Start_date` DATE,
`Contract End_date` DATE,
`Estemated Cost` INT NOT NULL
);


CREATE TABLE `Manager Table` (
`Manager id` INT PRIMARY KEY,
`First Name` TEXT NOT NULL,
`Last Name` TEXT
);


CREATE TABLE `Manager-Staff Table`(
`id` INT PRIMARY KEY,
`Contractid` INT NOT NULL,
`Manager id` INT NOT NULL,
`Staff id` INT NOT NULL,
`Devolping deadline` DATE,
`Testing deadline` DATE,
`Production deadline` DATE,
`Completion Date` DATE
);


CREATE TABLE `Staff Table`(
`Staff id` INT PRIMARY KEY,
`Staff Name` TEXT NOT NULL,
`Staff location_id` INT
);


CREATE TABLE `Staff Location Table`(
`Staff location_id` INT PRIMARY KEY,
`location` TEXT NOT NULL,
`State` TEXT,
`City` TEXT,
`Zip code` INT
);


ALTER TABLE `Manager-Staff Table`
ADD FOREIGN KEY (`Contractid`) REFERENCES `Contract Table`(`Contractid`),
ADD FOREIGN KEY (`Manager id`) REFERENCES `Manager Table`(`Manager id`),
ADD FOREIGN KEY (`Staff id`) REFERENCES `Staff Table`(`Staff id`);


ALTER TABLE `Contract Table`
ADD FOREIGN KEY (`Manager id`) REFERENCES `Manager Table`(`Manager id`),
ADD FOREIGN KEY (`Client id`) REFERENCES `Client Table`(`Client id`);


ALTER TABLE `Staff Table`
ADD FOREIGN KEY (`Staff location_id`) REFERENCES `Staff Location Table`(`Staff location_id`);

-- Drop database Project2;
