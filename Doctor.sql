CREATE DATABASE Project2;
USE Project2;

CREATE TABLE `Patient`(
`Patient id` INT PRIMARY KEY,
`First Name` TEXT NOT NULL,
`Last Name` TEXT ,
`Date of Birth` DATE NOT NULL,
`Address` TEXT,
`City` TEXT,
`Zip code` INT,
`State` TEXT
);

CREATE TABLE `Doctor`(
`Doctor id` INT PRIMARY KEY,
`First Name` TEXT,
`Last Name` TEXT,
`Secratory Name` TEXT
);

CREATE TABLE `Prescription`(
`id` INT PRIMARY KEY,
`Patient id` INT,
`Doctor id` INT,
`Prescribed Date` DATE,
`Drug` TEXT,
`Dosage` FLOAT(2,2)
);

ALTER TABLE `Prescription`
 Add FOREIGN KEY (`Patient id`) REFERENCES `Patient`(`Patient id`),
 Add FOREIGN KEY (`Doctor id`) REFERENCES `Doctor`(`Doctor id`);

-- Drop database Project2;