CREATE DATABASE Project2;
USE Project2;

CREATE TABLE `Patient`(
`Patient id` INT PRIMARY KEY,
`First Name` TEXT NOT NULL,
`Last Name` TEXT ,
`Date of Birth` DATE NOT NULL,
`Address` TEXT,
`City` TEXT,
`Zip code` INT,
`State` TEXT
);

CREATE TABLE `Concultant Table`(
`Concultant id` INT PRIMARY KEY,
`Patient id` INT,
`Doctor Name` TEXT,
`Secretary` TEXT,
`Prescription Date` DATE,
`Diagnosis` TEXT,
`Prescription` TEXT,
`Drug` TEXT,
`Dosage` FLOAT(2,2)
);

ALTER TABLE `Concultant Table`
 Add FOREIGN KEY (`Patient id`) REFERENCES `Patient`(`Patient id`);
 
 -- Drop database Project2;