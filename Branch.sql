CREATE DATABASE Project2;
USE Project2;


CREATE TABLE `Author Table`(
`Author id` INT PRIMARY KEY,
`Author Name` TEXT NOT NULL);


CREATE TABLE `Book Table`(
`ISBN` INT PRIMARY KEY,
`Title` TEXT NOT NULL,
`Author id` INT NOT NULL,
`Number of Copies` INT NOT NULL);


CREATE TABLE `Publisher Table`(
`Publisher id` INT PRIMARY KEY,
`Publisher Name` TEXT NOT NULL);


CREATE TABLE `Branch Table`(
`Branch id` INT PRIMARY KEY,
`Branch Name` TEXT,
`Branch Address` TEXT,
`city` TEXT NOT NULL,
`State` TEXT NOT NULL,
`Zip Code` INT NOT NULL
);


CREATE TABLE `Publish-Branch`(
`id` INT PRIMARY KEY,
`ISBN` INT NOT NULL,
`Branch id` INT NOT NULL,
`Publisher id` INT NOT NULL);


ALTER TABLE `Publish-Branch`
ADD FOREIGN KEY (`ISBN`) REFERENCES `Book Table`(`ISBN`),
ADD FOREIGN KEY (`Branch id`) REFERENCES `Branch Table`(`Branch id`),
ADD FOREIGN KEY (`Publisher id`) REFERENCES `Publisher Table`(`Publisher id`);


ALTER TABLE `Book Table`
ADD FOREIGN KEY(`Author id`) REFERENCES `Author Table`(`Author id`);
-- DROP DATABASE Project2;
